package utilities;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.util.HashMap;


public class ExcelOperations {
    private String filePath = System.getProperty("user.dir")+PropOperations.getPropValueByKey("testData");
    Sheet sh;

    public ExcelOperations(String fName ,String sheetName) {
        File testDataFile = new File(filePath+fName+".xlsx");
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(testDataFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sh = wb.getSheet(sheetName);
    }

    public HashMap<String, String> getTestDataInMap(int rowNum) throws Exception {
        HashMap<String, String> hm = new HashMap<String, String>();

        for (int i = 0; i < sh.getRow(0).getLastCellNum(); i++) {
            String value;
            if(sh.getRow(rowNum).getCell(i) != null) {
                sh.getRow(rowNum).getCell(i).setCellType(CellType.STRING);
                value = sh.getRow(rowNum).getCell(i).toString();
            }
            else {
                value = "";
            }
            hm.put(sh.getRow(0).getCell(i).toString(), value);
        }
        return hm;
    }

    public int getRowCount() {
        return sh.getLastRowNum();
    }

    public int getColCount() {
        return sh.getRow(0).getLastCellNum();

    }
}
