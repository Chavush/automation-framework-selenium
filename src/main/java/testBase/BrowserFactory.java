package testBase;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {

    public BrowserFactory(){

    }
    public WebDriver createBrowserInstance(String browser) {

        WebDriver driver = null;

        if(browser.equalsIgnoreCase("Chrome")) {

            WebDriverManager.chromedriver().setup();
            System.setProperty("webdriver.chrome.silentOutput", "true");
            driver = new ChromeDriver();

        } else if (browser.equalsIgnoreCase("firefox")) {

            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();

        } if (browser.equalsIgnoreCase("ie")) {

            WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();

        }

        return driver;
    }
}
