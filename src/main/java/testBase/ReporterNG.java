package testBase;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewName;
import utilities.PropOperations;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReporterNG {

    static ExtentReports extent;

    public static ExtentReports setupExtentReport() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyy HH-mm-ss");
        Date date = new Date();
        String actualDate = format.format(date);

        String reportPath = System.getProperty("user.dir") +
                "/Reports/ExecutionReport_" + actualDate + ".html";

        ExtentSparkReporter sparkReport = new ExtentSparkReporter(reportPath);

        extent = new ExtentReports();

        sparkReport.config().setDocumentTitle("Test report " + actualDate);
        sparkReport.config().setTheme(Theme.STANDARD);
        sparkReport.config().setReportName("Test report " + actualDate);
        sparkReport.viewConfigurer().viewOrder().as(new ViewName[] {
                        ViewName.DASHBOARD,
                        ViewName.TEST,
                        ViewName.AUTHOR,
                        ViewName.DEVICE,
                        ViewName.EXCEPTION,
                        ViewName.LOG
                })
                .apply();

        extent.attachReporter(sparkReport);
        extent.setSystemInfo("Executed on Environment: ", PropOperations.getPropValueByKey("url"));
        extent.setSystemInfo("Executed on Browser: ", PropOperations.getPropValueByKey("browser"));
        extent.setSystemInfo("Executed on OS: ", System.getProperty("os.name"));
        extent.setSystemInfo("Executed by User: ", System.getProperty("user.name"));

        return extent;
    }
}
