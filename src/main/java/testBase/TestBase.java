package testBase;


import utilities.PropOperations;

import java.util.concurrent.TimeUnit;

public class TestBase {
    static BrowserFactory bf = new BrowserFactory();

    public static void setUp(){
        String browser = PropOperations.getPropValueByKey("browser");
        String url = PropOperations.getPropValueByKey("url");

        DriverFactory.getInstance().setDriver(bf.createBrowserInstance(browser));

        DriverFactory.getInstance().getDriver().manage().window().maximize();
        DriverFactory.getInstance().getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        DriverFactory.getInstance().getDriver().navigate().to(url);
    }

    public static void tearDown() {
        DriverFactory.getInstance().closeBrowser();
    }
}
