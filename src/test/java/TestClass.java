import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import utilities.ExcelOperations;

import java.util.HashMap;

public class TestClass {

    ExcelOperations excel = new ExcelOperations("credentials", "Sheet1");
    @BeforeMethod
    public void setUp() {
        TestBase.setUp();
    }

    @DataProvider (name = "testData")
    public Object[][] testDataSupplier() throws Exception {
        Object[][] obj = new Object[excel.getRowCount()][1];
        for (int i = 1; i <= excel.getRowCount(); i++) {
            HashMap<String, String> testData = excel.getTestDataInMap(i);
            obj[i-1][0] = testData;
        }
        return obj;

    }

    @Test(dataProvider = "testData")
    public void Test1 (Object obj1) {
        HashMap<String, String> testData = (HashMap<String, String>) obj1;
        System.out.println("Test 1");
        System.out.println(testData.get("pass"));
    }
    @Test
    public void Test2 () {
        System.out.println("Test 2");
    }
    @Test
    public void Test3 () {
        System.out.println("Test 3");
    }
    @Test
    public void Test4 () {
        System.out.println("Test 4");
    }
    @Test
    public void Test5 () {
        Assert.fail("Test Fail");
        System.out.println("Test 5");
    }


    @AfterMethod
    public void tearDown() {
        TestBase.tearDown();
    }
}
